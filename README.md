# IFixit

The goal of this project is to help automate a part of the IFIXIT initiative to crowdsource repair information for medical equipment (See the [background section](#background) for more info.

First, we want to automatically download service manuals from a list of source and upload them to the correct product page on IFIXIT's website.

## Installing

Using python `virtualenv` is the best way to manage dependencies for a project.

### Setting up a vritual environment

Do this only once at the start of the project
```
pip install --user virtualenv
cd ifixit_respirators_auto_dl-ul
python -m virtualenv venv
```

### Entering the virtual environment

Do this everytime you want to work on the project

```
cd ifixit_respirators_auto_dl-ul
source venv/bin/activate
```

### Installing dependencies

Do this once at the start of the project. NOTE: you need to enter the virtual environment first

```
pip install -r requirements.txt
```

## Using the script

As of now, this is a Work In Progress and isn't usable without significant modifications. See the [contributing section](#contributing) if you want to help!

Copy `config.sample.yml` to `config.yml` and enter your ifixit.com session cookie in the specified field.

## Contributing

Contributions are welcome. Please develop using feature branches and open Merge Requests to have your code integrated. The most pressing changes right now are:

 - Factoring the parsing code into a separate class that is extensible for every source
 - Adding parsers for other sources in the google docs
 - Splitting the code into module files
 - Prevent upload of duplicate manuals
 - Better logging
 - Testing
 - Create pages for products that don't exist on the site (not sure?)

## Background

 - [Original article with information on the initiative](https://meta.ifixit.com/Answers/View/14993/Help+Us+Crowdsource+Repair+Information+for+Hospital+Equipment!)
 - [Discussion thread](https://www.ifixit.com/News/36354/help-us-crowdsource-repair-information-for-hospital-equipment)
 - [Google Doc sheet with tasks and resources](https://docs.google.com/spreadsheets/d/1Fz8ljtGv0XK0BkNtrO94xYYQmcuUPLjP-gl5kZMwuNE/)
