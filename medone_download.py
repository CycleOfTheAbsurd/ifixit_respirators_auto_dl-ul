#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import requests
import logging
import yaml

from os import remove
from os.path import abspath
from bs4 import BeautifulSoup
from enum import Enum
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

## Globals
with open("respirator_brand_list.txt") as infile:
    make_list = infile.readlines()

logging.basicConfig(filename = "resp_manual.log")
LOGGER = logging.getLogger("manual")
BASE_URL_TEMPLATE = "https://www.ifixit.com/Device/Edit/{}_Ventilator"


## Classes
class ManualType(Enum):
    SERVICE = "service manual"
    USER = "user manual"

class Manual():
    def __init__(self, name, url, man_type, make=None, dest_url=None):
        self.source_url = url
        self.type = man_type
        self.file = None
        self.dest_url = dest_url
        if make is not None:
            self.set_make_model(make=make, model=name)
        else:
            self.set_make_model(full=name)

    # Provide either fullname OR make and model
    def set_make_model(self, full=None, make=None, model=None):
        if make is not None:
            self.make = make
        if model is not None:
            self.model = make
        if full is not None:
            full = full.replace("-", " ")
            for m in make_list:
                if full.startswith(m.replace("-", " ")):
                    self.make = m
                    self.model = full[len(make) + 1:]
                    break
            else:
                self.model = full
                self.make = None

    def _prepare_for_upload(self):
        ## Prepare Contents
        if self.content is None:
            self._download()
        if make is None:
            self.make = input("Input the make for " + self.model + ": ")
            model = input("Input the make for " + self.model + "(enter to keep current model name): ")
            if model != "":
                self.model = model

        ## Prepare destination URL
        if self.dest_url is None:
            self.dest_url = BASE_URL_TEMPLATE.format(self.make.replace(" ", "_"))
            r = requests.get(self.dest_url)
            if r.status_code != 200:
                LOGGER.error("Could not find destination URL for {}. Tried {} and got status {}".format(self.model, self.dest_url, r.status_code))
                r.raise_for_status()

    def _download(self):
        self.content = requests.get(self.source_url).content()


class IFixItUploader():
    def __init__(self):
        with open("config.yml") as config:
            cookie_value = yaml.load(config)["cookie"]
        cookie_dict={
                "name": "session",
                "value": cookie_value,
                "domain": ".ifixit.com",
                "expires": "",
                "path": "/",
                "httpOnly": True,
                "HostOnly": False,
                "Secure": True
        }
        self.browser = webdriver.Firefox()
        self.browser.get("https://www.ifixit.com")
        self.browser.add_cookie(cookie_dict)


    def upload_manual(self, manual):
        try:
            manual._prepare_for_upload()
        except HTTPError as e:
            pass
        else:
            # Open the "Add a document" modal
            browser.get(manual.dest_url)
            browser.find_element_by_class_name("js-doc-upload-link").click()
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.ID, "addMediaButton"))
            ).click()
            # Save PDF to disk temporarily
            filename = manual.source_url.split("/")[-1]
            with open(filename, "wb") as tmpfile:
                tmpfile.write(manual.content)
            # Upload file, submit and save
            input_field = browser.find_element_by_class_name("fd-file")
            input_field.send_keys(abspath(filename))
            remove(tmpfile)
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, "[title='{}']".format(filename)))
            ).click()
            browser.find_element_by_id("quicksave").click()


# Most of the following code should be inside a sort of parser class
## e.g. MedOneParser(SiteParser)
def li_a_to_list(li_soup):
    return [(a.string, a.get("href")) for a in li_soup.findAll("a")]


URL = "https://www.medonegroup.com/resources/equipmentmanuals"

# Get all links to manual PDFs
page = requests.get(URL)
soup = BeautifulSoup(page.content, 'html.parser')
user_man_soup, service_man_soup = soup.findAll("div", {"class": "manual-column"})

manuals = []
for u in li_a_to_list(user_man_soup):
    manuals.append(Manual(u[0], u[1], ManualType.USER))
for s in li_a_to_list(service_man_soup):
    manuals.append(Manual(u[0], u[1], ManualType.SERVICE))
